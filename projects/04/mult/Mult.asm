// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)
//
// This program only needs to handle arguments that satisfy
// R0 >= 0, R1 >= 0, and R0*R1 < 32768.

// Put your code here.

//for i < n:n+n;

    @i
    M=0
    @2
    M=0

(LOOP)
    //if (i == n ) go to END
    @i
    D=M 
    @R0
    D=D-M

    @END
    D;JEQ


    // R2 = R2 + R1  repeat n times
    @R1
    D=M

    @R2
    D=D+M
    M=D
    

    //i++
    @i
    D=M
    D=D+1
    M=D

    @LOOP
    0;JEQ


(END)