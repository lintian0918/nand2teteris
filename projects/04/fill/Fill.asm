// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.

//if key > 0: fill screen
//else screen =0 

(BEGIN)
    @SCREEN
    D=A
    @adress
    M=D
    @24575
    D=A
    @end
    M=D

    @24576
    D=M
    @key
    M=D
    D=M
    @CLEAN
    D;JEQ
    // if key == 0, clean the screen 
(ONKEY)



    @adress
    A=M
    M=-1

    @adress
    M=M+1
    D=M

    @end
    D=D-M
    @BEGIN
    D;JEQ
    @ONKEY
    0;JEQ


    
(CLEAN)

    @adress
    A=M
    M=0

    @adress
    M=M+1
    D=M

    @end
    D=D-M
    
    @BEGIN
    D;JEQ

    @CLEAN
    0;JEQ
