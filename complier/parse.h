/*this program purpose is to deal with to translate the asm flie to binary code.
parse is ording to recongize the asm language and divide them to three part if 
instructions is C-instructions.It will be divide to two part if instruction is A-instrution.
the asm specifitions follow the sepcification*/

#pragma once
#define _PARSE_H_
#ifdef  _PARSE_H_

#include <iostream>
#include <vector> 
#include <fstream>
#include <string>
#include <bitset>
#include <algorithm>
#include <regex> 

#include "symbols.h"

using namespace std;

//load file to remove the space about some  \n \000 and // output to source files
vector<string> load_flie(string file_name){
    
    vector<string> out;
    ifstream       fin(file_name);
    string         line;
    
    while (getline(fin, line)) // read flie line by line
    {
        //remove all of spaces in the texts.
        line.erase(remove_if(line.begin(), line.end(), [](char c) { return isspace(c); }), line.end());
        char words[2] = {line[0],line[1]};
        size_t pos = line.find('/');
        //cheout the first of lines word,Exclude spaces and double slashes.
        if (words[0] == '\n' || words[0] == '\000'|| (words[0] == '/' && words[1] == '/'))
        {
            continue;
        }
        else if(pos != string::npos)
        {
            line.erase(pos);
            out.push_back(line);
        }
        else
        {
            out.push_back(line);
        }
    }

    fin.close();
    return out;
}

int classify_ins(string line )
{
    
    //Output 0 if instruction is a A-ins,else oput 1; 
    if(line[0] == '@') return 0;
    else return 1;
}

bool parse_a_ins(string line){
    // a-ins do nothing
    return 1;
}

vector<string> parse_c_ins(string line)
{
    //spilt the c-instructions to three part.
    regex pattern("[=;]");  
    vector<string> tokens{sregex_token_iterator(line.begin(), line.end(), pattern, -1),
    sregex_token_iterator() };
    return tokens;
}

vector<vector<string>> parse(string file_name)
{
    /*args : the trace of the file
    the func will porcess the file.asm and output a vector<vector<string>> type variable,which contain the a-ins 
    and three part of c-ins*/

    vector<vector<string>> out;
    
    //remove the blank spaces and note of file
    vector<string> ins = load_flie(file_name);

    //to mapping symbols
    vector<string> first_scan = line_symbols_sacn(ins);
    vector<string> text = preset_symbol_scan(first_scan);


    for (string line : text)
    {
        int label = classify_ins(line);        //check out the ins type.
        if(label == 1){
            out.push_back(parse_c_ins(line));  //if c-ins,it's spilted three parts. 
        }
        else{
            vector<string> lines {line};       //if a-ins,nothing.
            out.push_back(lines);
        }
    }

    return out;
}
#endif //_PARSE_H_