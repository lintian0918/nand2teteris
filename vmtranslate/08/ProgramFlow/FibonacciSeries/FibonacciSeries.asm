//push argument 1
//D = RAM[argument + i]
@1
D=A
@ARG
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop pointer1
//THIS OR THAT = RAM[SP--]
//SP--
@SP
M=M-1
A=M
D=M
@THAT
M=D
//push constant 0
//RAM[SP] = i
@0
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop that 0
//SP--
@SP
M=M-1
//R13 = 0i
@0
D=A
@THAT
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//push constant 1
//RAM[SP] = i
@1
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop that 1
//SP--
@SP
M=M-1
//R13 = 1i
@1
D=A
@THAT
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//push argument 0
//D = RAM[argument + i]
@0
D=A
@ARG
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push constant 2
//RAM[SP] = i
@2
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//sub
//SP--
@SP
M=M-1
//D=RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] -RAM[SP-1]
A=M
M=M-D
//SP++
@SP
M=M+1
//pop argument 0
//SP--
@SP
M=M-1
//R13 = 0i
@0
D=A
@ARG
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//labelMAIN_LOOP_START
(MAIN_LOOP_START)
//push argument 0
//D = RAM[argument + i]
@0
D=A
@ARG
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//if-gotoCOMPUTE_ELEMENT
//SP--
@SP
M=M-1
@SP
A=M
D=M 
@COMPUTE_ELEMENT
D;JNE
@END_PROGRAM
0;JMP
//labelCOMPUTE_ELEMENT
(COMPUTE_ELEMENT)
//push that 0
//D = RAM[that + i]
@0
D=A
@THAT
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push that 1
//D = RAM[that + i]
@1
D=A
@THAT
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//add
//SP--
@SP
M=M-1
//D = RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] + RAM[SP-1]
A=M
M=D+M
//SP++
@SP
M=M+1
//pop that 2
//SP--
@SP
M=M-1
//R13 = 2i
@2
D=A
@THAT
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//push pointer1
//RAM[SP] = THIS OR THAT
@THAT
D=M
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push constant 1
//RAM[SP] = i
@1
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//add
//SP--
@SP
M=M-1
//D = RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] + RAM[SP-1]
A=M
M=D+M
//SP++
@SP
M=M+1
//pop pointer1
//THIS OR THAT = RAM[SP--]
//SP--
@SP
M=M-1
A=M
D=M
@THAT
M=D
//push argument 0
//D = RAM[argument + i]
@0
D=A
@ARG
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push constant 1
//RAM[SP] = i
@1
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//sub
//SP--
@SP
M=M-1
//D=RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] -RAM[SP-1]
A=M
M=M-D
//SP++
@SP
M=M+1
//pop argument 0
//SP--
@SP
M=M-1
//R13 = 0i
@0
D=A
@ARG
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
@MAIN_LOOP_START
0;JMP
//labelEND_PROGRAM
(END_PROGRAM)
