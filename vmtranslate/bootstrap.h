#pragma once 
#define _BOOTSTRAP_H_
#ifdef _BOOTSTRAP_H_

#include "instruction.h"


#define SPUP "//SP++\n@SP\nM=M+1\n"
#define SPDM "//SP--\n@SP\nM=M-1\n"

#include <string>

using namespace std;


// Bootstrap class is begin of the all assembly code
// it's begin of the program,so it will be written in the line 0
class Bootstrap : public Instruction
{
public:
    string output(int index) override
    {
        string out = "";
        out += "@256\nD=A\n@SP\nM=D\n";
        out += "@LCL\nM=-1\n";
        out += "@1\nD=A\n@LCL\nD=M-D\n@ARG\nM=D\n";
        out += "@2\nD=A\n@LCL\nD=M-D\n@THIS\nM=D\n";
        out += "@3\nD=A\n@LCL\nD=M-D\n@THAT\nM=D\n";
        // Call Sys.init
        // push retAdress
        out += "@ret$Sys.init\n";
        out += "D=A\n@SP\nA=M\nM=D\n";
        //SP++,push LCL
        out += SPUP;
        out += "@LCL\nD=M\n@SP\nA=M\nM=D\n";
        //SP++,push ARG
        out += SPUP;
        out += "@ARG\nD=M\n@SP\nA=M\nM=D\n";
        //SP++,push This
        out += SPUP;
        out += "@THIS\nD=M\n@SP\nA=M\nM=D\n";
        //SP++,push That
        out += SPUP;
        out += "@THAT\nD=M\n@SP\nA=M\nM=D\n";
        //SP++
        out += SPUP;
        //ARG = SP - 5 is retAdress
        out += "@SP\nD=M\n@5\nD=D-A\n@ARG\nM=D\n";
        //LCL = SP
        out += "@SP\nD=M\n@LCL\nM=D\n";
        // jump to Sys.init
        out += "@Sys.init\n0;JMP\n";
        // return adress
        out += "(ret$Sys.init)\n";
        return out;
    }
};




#endif // _BOOTSTRAP_H_