#pragma once
#define _FUNCTIONCALL_H_
#ifdef _FUNCTIONCALL_H_

#include "instruction.h"


#define SPUP "//SP++\n@SP\nM=M+1\n"
#define SPDM "//SP--\n@SP\nM=M-1\n"

using namespace std;

// Base class for all function call instructions
// it contains the all of the implementation in function call,function return and function function.
// more details in the website: https://www.nand2tetris.org/project08
class FunctionCall : public Instruction
{
    public:
    virtual string output(int index) = 0;
};

class Call : public FunctionCall
{
public:
    string output(int index) override
    {
        string out ="";
        string idx = to_string(index);
        // Call function nargs
        out += "// Call " + function_name_ + idx + "\n";
        // push ret adress
        out += "@ret$" + function_name_+ to_string(id) +"\n";
        out += "D=A\n@SP\nA=M\nM=D\n";
        //SP++,push LCL
        out += SPUP;
        out += "@LCL\nD=M\n@SP\nA=M\nM=D\n";
        //SP++,push ARG
        out += SPUP;
        out += "@ARG\nD=M\n@SP\nA=M\nM=D\n";
        //SP++,push This
        out += SPUP;
        out += "@THIS\nD=M\n@SP\nA=M\nM=D\n";
        //SP++,push That
        out += SPUP;
        out += "@THAT\nD=M\n@SP\nA=M\nM=D\n";
        //SP++
        out += SPUP;
        //ARG = SP - 5 - nArg
        out += "@SP\nD=M\n@5\nD=D-A\n@" + idx + "\nD=D-A\n@ARG\nM=D\n";
        //LCL = SP
        out += "@SP\nD=M\n@LCL\nM=D\n";
        // goto functionName
        out += "@" + function_name_+ "\n";
        out += "0;JMP\n";
        // retrun AdressLabel
        out += "(ret$" + function_name_ + to_string(id) +")\n";
        return out;
    }

    Call(string function_name, int id)
    {
        function_name_ = function_name;
        this->id = id;
    }
    
    string function_name_;
    int id;
};

class Function :public FunctionCall
{
public:
    string output(int index) override
    {
        string out ="// Function " + function_name_ + to_string(index) +"\n";
        out += "(" + function_name_ + ")\n";
        //function Functionsname aArgs
        for (int i = 0; i < index ; i ++ )
        {
            out += "@SP\nA=M\nM=0\n";
            out += SPUP;
        }
        return out;
    }
    
    Function(string function_name)
    {
        function_name_ = function_name;
    }

    string function_name_;
};

class Return : public FunctionCall
{
    public:
    string output(int index) override
    {
        string out = "//Return\n";
        // endFrame = LCL
        out += "@LCL\nD=M\n@R13\nM=D\n";
        //retAddr =*(endFrame - 5)
        out += "@R13\nD=M\n@5\nD=D-A\nA=D\nD=M\n@R14\nM=D\n";
        //*ARG = pop()
        out += SPDM;
        out += "@SP\nA=M\nD=M\n@ARG\nA=M\nM=D\n";
        //SP = ARG + 1
        out +="@ARG\nD=M+1\n@SP\nM=D\n";
        //That  = *(endFrame - 1)
        out += "@R13\nD=M\n@1\nD=D-A\nA=D\nD=M\n@THAT\nM=D\n";
        //This  = *(endFrame - 2)
        out += "@R13\nD=M\n@2\nD=D-A\nA=D\nD=M\n@THIS\nM=D\n";
        //ARG   = *(endFrame - 3)
        out += "@R13\nD=M\n@3\nD=D-A\nA=D\nD=M\n@ARG\nM=D\n";
        //LCL   = *(endFrame - 4)
        out += "@R13\nD=M\n@4\nD=D-A\nA=D\nD=M\n@LCL\nM=D\n";
        //goto retAddr  
        out += "@R14\nA=M\n0;JMP\n"; 
        return out;
    }
};


#endif // _FUNCTIONCALL_H_