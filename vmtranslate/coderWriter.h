#pragma once
#define _CODERWRITER_H_
#ifdef  _CODERWRITER_H_

#include "pushpop.h"
#include "arthmetic.h"
#include "branch.h"
#include "functioncall.h"

using namespace std;
class coderWriter
{
    public:
    void set_instruction(unique_ptr<Instruction>& instruction)
    {
        this->instruction = move(instruction);
    }
    
    string write_instruction(int index)
    {
        if(instruction){
            return instruction->output(index);
        }
    }
    private:
    unique_ptr<Instruction> instruction;
};


// call the function output of the instruction
string write_instruction(unique_ptr<Instruction>& instruction,int index)
{
    return instruction->output(index);
}

#endif //_CODERWRITER_H_