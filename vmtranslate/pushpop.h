#pragma once
#define _PUSHPOP_H_
#ifdef _PUSHPOP_H_

#include <string>
#include "instruction.h"
using namespace std;

#define SPUP "//SP++\n@SP\nM=M+1\n"
#define SPDM "//SP--\n@SP\nM=M-1\n"

// Base class for all push instructions
class Push:public Instruction {
public:
    virtual string output(int index) = 0;
};

// Base class for all pop instructions
class Pop:public Instruction{
public:
    virtual string output(int index) = 0;
};

// push constant i
class PushConstant :public Push {
public:
    string output(int index) override {
        string out = "";
        string idx = to_string(index);
        //push comstant idx
        out+="//push constant " + idx +"\n";
        out+="//RAM[SP] = i\n";

        out+="@" + idx +"\n";  //@idx
        out+="D=A\n";

        out+="@SP\n";
        out+="A=M\n";
        out+="M=D\n";
        out+=SPUP;
        return out;
    }
};

// push local i
class PushLocal :public Push {
public:
    string output(int index) override
    {
        string out = "";
        string idx = to_string(index);
        out += "//push local " + idx + "\n";  
        //push local i

        //D = RAM[local + i]
        out += "//D = RAM[local + i]\n";
        
        out += "@" + idx + "\n";  //@idx
        out += "D=A\n";
        
        out += "@LCL\n";    //@seg
        out += "A=D+M\n";
        out += "D=M\n";
        
        // RAM[SP] = D
        out += "//RAM[SP] = D\n";
        out += "@SP\n";
        out += "A=M\n";
        out += "M=D\n";
        out += SPUP;
        return out;
    }
};

// push argument i
class PushArgument :public Push  {
public:
    string output(int index) override
    {
        string out = "";
        string idx = to_string(index);
        out += "//push argument " + idx + "\n";  
        //push argument i

        //D = RAM[argument + i]
        out += "//D = RAM[argument + i]\n";
        
        out += "@" + idx + "\n";  //@idx
        out += "D=A\n";
        
        out += "@ARG\n";    //@seg
        out += "A=D+M\n";
        out += "D=M\n";
        
        // RAM[SP] = D
        out += "//RAM[SP] = D\n";
        out += "@SP\n";
        out += "A=M\n";
        out += "M=D\n";
        out += SPUP;
        return out;
    }
};

// push this i
class PushThis :public Push {
public:
    string output(int index) override
    {
        string out = "";
        string idx = to_string(index);
        out += "//push this " + idx + "\n";  
        //push this i

        //D = RAM[this + i]
        out += "//D = RAM[this + i]\n";
        
        out += "@" + idx + "\n";  //@idx
        out += "D=A\n";
        
        out += "@THIS\n";    //@seg
        out += "A=D+M\n";
        out += "D=M\n";
        
        // RAM[SP] = D
        out += "//RAM[SP] = D\n";
        out += "@SP\n";
        out += "A=M\n";
        out += "M=D\n";
        out += SPUP;
        return out;
    }
};

// push that i
class PushThat :public Push {
public:
    string output(int index) override
    {
        string out = "";
        string idx = to_string(index);
        out += "//push that " + idx + "\n";  
        //push that i

        //D = RAM[that + i]
        out += "//D = RAM[that + i]\n";
        
        out += "@" + idx + "\n";  //@idx
        out += "D=A\n";
        
        out += "@THAT\n";    //@seg
        out += "A=D+M\n";
        out += "D=M\n";
        
        // RAM[SP] = D
        out += "//RAM[SP] = D\n";
        out += "@SP\n";
        out += "A=M\n";
        out += "M=D\n";
        out += SPUP;
        return out;
    }
};

// push pointer i
class PushPointer :public Push {
public:
    string output(int index) override
    {
        string out = "";
        //push the base adress of this and that into the stack
        //push pointer idx
        string idx = to_string(index);
        out+="//push pointer" + idx +"\n";  
        //RAM[SP] = THIS
        out+="//RAM[SP] = THIS OR THAT\n";
        if(index == 0 )
        {
            out+="@THIS\n";
        }
        else out+="@THAT\n";
        //D = THIS OR THAT
        out+="D=M\n";
        //RAM[SP] = D
        out+="@SP\n";
        out+="A=M\n";
        out+="M=D\n";
        out+=SPUP;
        return out;
    }
};

// push temp i
class PushTemp :public Push {
public:
    string output(int index) override
    {
        string out = "";
        string idx = to_string(index);
        out+="//push temp " + idx+"\n";
        //D = i
        out+="@" + idx + "\n";
        out+="D=A\n";
        //D = RAM[temp + i]
        out+="//D = RAM[5]\n";
        out+="@5\n";
        out+="A=D+A\n";
        out+="D=M\n";
        //RAM[SP] = D 
        out+="//RAM[SP] = D\n";
        out+="@SP\n";
        out+="A=M\n";
        out+="M=D\n";
        //SP++
        out+=SPUP;
        return out;
    }
};

// push static i
class PushStatic :public Push {
public:
    string output(int index) override
    {
        string out = "";
        string idx = to_string(index);
        out+="//push static " + idx+"\n";
        //D = i
        out+="@" + idx + "\n";
        out+="D=A\n";
        //D = RAM[static + i]
        out+="//D = RAM[16]\n";
        out+="@16\n";
        out+="A=D+A\n";
        out+="D=M\n";
        //RAM[SP] = D 
        out+="//RAM[SP] = D\n";
        out+="@SP\n";
        out+="A=M\n";
        out+="M=D\n";
        //SP++
        out+=SPUP;
        return out;
    }
};

// pop constant i
class PopLocal :public Pop {
public:
    string output(int index) override
    {
        string out = "";
        string idx = to_string(index);
        //pop local i 
        out += "//pop local " + idx + "\n"; 
        out += SPDM;

        //R13 = reg_idx + i
        out += "//R13 = " + idx + "i\n";
        out += "@" + idx + "\n";    //@idx
        out += "D=A\n";      
        out += "@LCL\n";      //@seg
        out += "D=D+M\n";
        out += "@R13\n";
        out += "M=D\n";

        //RAM[R13] == RAM[SP]
        out += "//RAM[R13] = RAM[SP]\n";;
        out += "@SP\n";
        out += "A=M\n";
        out += "D=M\n";
        out +="@R13\n";
        out += "A=M\n";
        out += "M=D\n";
        return out;
    }
};

// pop argument i
class PopArgument :public Pop {
public:
    string output(int index) override
    {
        string out = "";
        string idx = to_string(index);
        //pop argument i 
        out += "//pop argument " + idx + "\n"; 
        out += SPDM;

        //R13 = reg_idx + i
        out += "//R13 = " + idx + "i\n";
        out += "@" + idx + "\n";    //@idx
        out += "D=A\n";      
        out += "@ARG\n";      //@seg
        out += "D=D+M\n";
        out += "@R13\n";
        out += "M=D\n";

        //RAM[R13] == RAM[SP]
        out += "//RAM[R13] = RAM[SP]\n";;
        out += "@SP\n";
        out += "A=M\n";
        out += "D=M\n";
        out +="@R13\n";
        out += "A=M\n";
        out += "M=D\n";
        return out;
    }
};

// pop this i
class PopThis :public Pop {
public:
    string output(int index) override
    {
        string out = "";
        string idx = to_string(index);
        //pop this i 
        out += "//pop this " + idx + "\n"; 
        out += SPDM;

        //R13 = reg_idx + i
        out += "//R13 = " + idx + "i\n";
        out += "@" + idx + "\n";    //@idx
        out += "D=A\n";      
        out += "@THIS\n";      //@seg
        out += "D=D+M\n";
        out += "@R13\n";
        out += "M=D\n";

        //RAM[R13] == RAM[SP]
        out += "//RAM[R13] = RAM[SP]\n";;
        out += "@SP\n";
        out += "A=M\n";
        out += "D=M\n";
        out +="@R13\n";
        out += "A=M\n";
        out += "M=D\n";
        return out;
    }
};

// pop that i
class PopThat :public Pop {
public:
    string output(int index) override
    {
        string out = "";
        string idx = to_string(index);
        //pop that i 
        out += "//pop that " + idx + "\n"; 
        out += SPDM;

        //R13 = reg_idx + i
        out += "//R13 = " + idx + "i\n";
        out += "@" + idx + "\n";    //@idx
        out += "D=A\n";      
        out += "@THAT\n";      //@seg
        out += "D=D+M\n";
        out += "@R13\n";
        out += "M=D\n";

        //RAM[R13] == RAM[SP]
        out += "//RAM[R13] = RAM[SP]\n";;
        out += "@SP\n";
        out += "A=M\n";
        out += "D=M\n";
        out +="@R13\n";
        out += "A=M\n";
        out += "M=D\n";
        return out;
    }
};

// pop pointer i
class PopTemp :public Pop {
public:
    string output(int index) override
    {
        string out = "";
        string idx = to_string(index);
        //SP--
        out+="//pop temp i \n";

        //D = [static + i]
        out+="//D = temp " + idx+ "\n"; 
        out+="@" + idx +"\n";  //@idx
        out+="D=A\n";
        out+="@5\n";   //@seg
        out+="D=D+A\n";
        //R13 = D
        out+="@R13\n";
        out+="M=D\n"; 

        //SP--
        out+=SPDM;
        //RAM[R13] = RAM[SP]
        out+="// RAM[R13] == RAM[SP] \n";
        out+="@SP\n";
        out+="A=M\n";
        out+="D=M\n";
        out+="@R13\n";
        out+="A=M\n";
        out+="M=D\n";
        return out;
    }
};

// pop pointer i
class PopPointer :public Pop {
public:
    string output(int index) override
    {
        string out = "";
        string idx = to_string(index);
        out+="//pop pointer" + idx +"\n";
        out+="//THIS OR THAT = RAM[SP--]\n";
        out+=SPDM;
        //D = RAM[SP--] 
        out+="A=M\n";
        out+="D=M\n";
        if(index == 0 )
        {
            out+="@THIS\n";
        }
        else out+="@THAT\n";
        //THIS == D
        out+="M=D\n";
        return out;
    }
};

// pop static i
class PopStatic :public Pop {
public:
    string output(int index) override
    {
        string out = "";
        string idx = to_string(index);


        //SP--
        out+="//pop static i \n";

        //D = [static + i]
        out+="//D = pop static " + idx+ "\n"; 
        out+="@" + idx +"\n";  //@idx
        out+="D=A\n";
        out+="@16\n";   //@seg
        out+="D=D+A\n";
        //R13 = D
        out+="@R13\n";
        out+="M=D\n"; 

        //SP--
        out+=SPDM;
        //RAM[R13] = RAM[SP]
        out+="// RAM[R13] == RAM[SP] \n";
        out+="@SP\n";
        out+="A=M\n";
        out+="D=M\n";
        out+="@R13\n";
        out+="A=M\n";
        out+="M=D\n";
        return out;
    }
};


#endif //_PUSHPOP_H_