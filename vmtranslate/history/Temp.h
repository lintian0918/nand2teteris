#pragma once
#define _PARSER_H_
#ifdef  _PARSER_H_

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <map>
#include "parser.h"

using namespace std;

//opens an output file/stream and gets ready to write into it.
#define output_file(file_name) ofstream fout(file_name)
#define close_file() fout.close()
#define SPUP "//SP++\n@SP\nM=M+1\n"
#define SPDM "//SP--\n@SP\nM=M-1\n"

//write the assembly code that is the translation of the given arithmetic command.
string write_arthmetic(string command,int index)
{
    string out = "";
    string str_idx = to_string(index);
    if (command == "add")
    {
        out +="//add\n";
        //load a variable of the SP
        out +=SPDM;
        out +="//D = RAM[SP]\n";
        out +="A=M\n";
        out +="D=M\n";
        out +=SPDM;
        // to do function add
        out +="//RAM[SP-1] = RAM[SP] + RAM[SP-1]\n";
        out +="A=M\n";
        out +="M=D+M\n";
        out +=SPUP;
    }
    else if(command == "sub")
    {
        out +="//sub\n";
        //load a variable of the SP
        out +=SPDM;
        out +="//D=RAM[SP]\n";
        out +="A=M\n";
        out +="D=M\n";

        out +=SPDM;
        out +="//RAM[SP-1] = RAM[SP] -RAM[SP-1]\n";
        out +="A=M\n";
        out +="M=M-D\n";
        out +=SPUP;
    }
    else if(command == "neg")
    {
        out +="//neg\n";

        out +=SPDM;
        out +="A=M\n";
        out +="M=-M\n";
        out +=SPUP;
    }
    else if(command == "eq")
    {
        out +="//eq\n";

        out +=SPDM;
        out +="A=M\n";
        out +="D=M\n";
        
        out +=SPDM;
        out +="A=M\n";
        // if D-M is 0 and jump to JP_1
        out +="D=M-D\n";
        out +="@EQ_1" + str_idx + "\n";
        out +="D;JEQ\n";
        
        // if D-M not 0 jump to JP_2
        out +="@SP\n";
        out +="A=M\n";
        out +="M=0\n";

        out +="@EQ_2" + str_idx + "\n";
        out +="0;JEQ\n";
        
        out +="(EQ_1" + str_idx + ")" + "\n";
        out +="@SP\n";
        out +="A=M\n";
        out +="M=-1\n";
        
        //load the memory 
        out +="(EQ_2" + str_idx + ")" + "\n";
        out +=SPUP;

    }
    else if(command == "gt")
    {
        out +="//gt\n";

        out +=SPDM;
        out +="A=M\n";
        out +="D=M\n";
        
        out +=SPDM;
        out +="A=M\n";
        // if M > D  and jump to GT_1
        out +="D=M-D\n";
        out +="@GT_1" + str_idx + "\n";
        out +="D;JGT\n";
        
        // if M<=D jump to GT_2
        out +="@SP\n";
        out +="A=M\n";
        out +="M=0\n";

        out +="@GT_2" + str_idx + "\n";
        out +="0;JEQ\n";
        
        out +="(GT_1" + str_idx + ")" + "\n";
        out +="@SP\n";
        out +="A=M\n";
        out +="M=-1\n";
        
        //load the memory 
        out +="(GT_2" + str_idx + ")" + "\n";
        out +=SPUP;

    }
    else if(command == "lt")
    {
        out +="//lt\n";

        out +=SPDM;
        out +="A=M\n";
        out +="D=M\n";
        
        out +=SPDM;
        out +="A=M\n";
        // if M < D and jump to LT_1
        out +="D=M-D\n";
        out +="@LT_1" + str_idx + "\n";
        out +="D;JLT\n";
        
        // if M >= D jump to LT_2
        out +="@SP\n";
        out +="A=M\n";
        out +="M=0\n";

        out +="@LT_2" + str_idx + "\n";
        out +="0;JEQ\n";
        
        out +="(LT_1" + str_idx + ")" + "\n";
        out +="@SP\n";
        out +="A=M\n";
        out +="M=-1\n";
        
        //load the memory 
        out +="(LT_2" + str_idx + ")" + "\n";
        out +=SPUP;

    }
    else if(command == "and")
    {
        out+= "//and\n";
        //SP--
        //load a variable of the SP
        out +=SPDM;
        out +="A=M\n";
        out +="D=M\n";
        out +=SPDM;
        // to do function and
        out +="A=M\n";
        out +="M=D&M\n";
        out +=SPUP;
    }
    else if(command == "or")
    {
        out +="//or\n";
        //SP--
        //load a variable of the SP
        out +=SPDM;
        out +="A=M\n";
        out +="D=M\n";
        out +=SPDM;
        // to do function add
        out +="A=M\n";
        out +="M=D|M\n";
        out +=SPUP;
    }
    else if(command == "not")
    {
        out +="//not\n";

        out +=SPDM;
        out +="A=M\n";
        out +="M=!M\n";
        out +=SPUP;
    }
    return out;
}

map<string,string> regs_map{{"local","LCL"},{"static","16"},{"temp","5"},{"this","THIS"},{"that","THAT"},
{"argument","ARG"}};

//write the assembly code that is the commandtype of the C_PUSH or C_POP
string write_push_pop(int commandtype,string segment,int seg_idx)   
{
    string out = "";

    //check out the type of segment
    bool is_const = segment == "constant";
    bool is_pointer = segment == "pointer";
    bool is_static_temp = segment =="static" | segment == "temp";
    bool is_LCL_THIS_TAHT_ARG = segment =="local"|segment == "local"|segment =="this"|segment =="that"|segment =="argument";
    

    //translate the seg_idx and segment to corret way
    string idx = to_string(seg_idx);
    string seg = regs_map[segment];
    //push pop local | argment | THIS | THAT
    if(commandtype == C_PUSH && is_LCL_THIS_TAHT_ARG){
        out += "//push " + segment +" " + idx + "\n";  
        //push local i

        //D = RAM[local + i]
        out += "//D = RAM[" + segment + "i]\n";
        
        out += "@" + idx + "\n";  //@idx
        out += "D=A\n";
        
        out += "@"+ seg +"\n";    //@seg
        out += "A=D+M\n";
        out += "D=M\n";
        
        // RAM[SP] = D
        out += "//RAM[SP] = D\n";
        out += "@SP\n";
        out += "A=M\n";
        out += "M=D\n";
        out += SPUP;
    }
    else if(commandtype == C_POP && is_LCL_THIS_TAHT_ARG)
    {
        //pop sengment i 
        out += "//pop " + segment +" " + idx + "\n"; 
        out += SPDM;

        //R13 = reg_idx + i
        out += "//R13 = " + idx + "i\n";
        out += "@" + idx + "\n";    //@idx
        out += "D=A\n";      
        out += "@"+ seg +"\n";      //@seg
        out += "D=D+M\n";
        out += "@R13\n";
        out += "M=D\n";

        //RAM[R13] == RAM[SP]
        out += "//RAM[R13] = RAM[SP]\n";;
        out += "@SP\n";
        out += "A=M\n";
        out += "D=M\n";
        out +="@R13\n";
        out += "A=M\n";
        out += "M=D\n";
    }
    //constant
    else if(commandtype == C_PUSH && is_const)
    {
        //push comstant idx
        out+="//push constant " + idx +"\n";
        out+="//RAM[SP] = i\n";

        out+="@" + idx +"\n";  //@idx
        out+="D=A\n";

        out+="@SP\n";
        out+="A=M\n";
        out+="M=D\n";
        out+=SPUP;
        
    }
    else if(commandtype == C_POP && is_const)
    {
        //nothing
    }
    //pointer 
    else if(commandtype == C_PUSH && is_pointer)
    {
        //push the base adress of this and that into the stack
        //push pointer idx
        out+="//push pointer" + idx +"\n";  
        //RAM[SP] = THIS
        out+="//RAM[SP] = THIS OR THAT\n";
        if(seg_idx ==0 )
        {
            out+="@THIS\n";
        }
        else out+="@THAT\n";
        //D = THIS OR THAT
        out+="D=M\n";
        //RAM[SP] = D
        out+="@SP\n";
        out+="A=M\n";
        out+="M=D\n";
        out+=SPUP;
    }
    else if(commandtype == C_POP && is_pointer)
    {
        out+="//pop pointer" + idx +"\n";
        out+="//THIS OR THAT = RAM[SP--]\n";
        out+=SPDM;
        //D = RAM[SP--] 
        out+="A=M\n";
        out+="D=M\n";
        if(seg_idx ==0 )
        {
            out+="@THIS\n";
        }
        else out+="@THAT\n";
        //THIS == D
        out+="M=D\n";
    }
    //static temp
    else if(commandtype == C_PUSH && is_static_temp)
    {
        out+="//push "+ segment +" " + idx+"\n";
        //D = i
        out+="@" + idx + "\n";
        out+="D=A\n";
        //D = RAM[static + i]
        out+="//D = RAM[" + seg + "]"+ "\n";
        out+="@" + seg +"\n";
        out+="A=D+A\n";
        out+="D=M\n";
        //RAM[SP] = D 
        out+="//RAM[SP] = D\n";
        out+="@SP\n";
        out+="A=M\n";
        out+="M=D\n";
        //SP++
        out+=SPUP;
    }
    else if(commandtype == C_POP && is_static_temp)
    {
        //SP--
        out+="//pop " + seg + " " +"\n";

        //D = [static + i]
        out+="//D = " + segment + idx+ "\n"; 
        out+="@" + idx +"\n";  //@idx
        out+="D=A\n";
        out+="@" +seg +"\n";   //@seg
        out+="D=D+A\n";
        //R13 = D
        out+="@R13\n";
        out+="M=D\n"; 

        //SP--
        out+=SPDM;
        //RAM[R13] = RAM[SP]
        out+="// RAM[R13] == RAM[SP] \n";
        out+="@SP\n";
        out+="A=M\n";
        out+="D=M\n";
        out+="@R13\n";
        out+="A=M\n";
        out+="M=D\n";
    }
    return out;
}

#endif  //_PARSER_H_