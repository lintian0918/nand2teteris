#pragma once
#define _BRANCH_H_
#ifdef _BRANCH_H_


#define SPUP "//SP++\n@SP\nM=M+1\n"
#define SPDM "//SP--\n@SP\nM=M-1\n"

#include "instruction.h"
#include <string>

using namespace std;

// Base class for all branch instructions
// branch instructions are label, goto and if-goto
class Branch : public Instruction
{
public:
    virtual string output(int index) = 0 ;
};

class LabelLabel : public Branch
{
public:
    LabelLabel(string label){
    this->label_ = label;
    }
    string output(int index) override
    {
        string out = "";
        out += "//label" + label_ + "\n";
        out += "(" + label_ + ")\n";
        return out;
    }
    string label_;
};

class GotoLabel : public Branch
{
public:

    GotoLabel(string label):label_(label){};
    string output(int index) override
    {
        string out = "";
        out += "@" + label_ + "\n";
        out += "0;JMP\n";
        return out;
    }

    string label_; 
};

class IfGotoLabel : public Branch
{
    public:
    IfGotoLabel(string label):label_(label){};
    
    string output(int index) override
    {
        string out = "";
        out += "//if-goto" + label_ + "\n";
        out += SPDM;
        out += "@SP\n";
        out += "A=M\n";
        out += "D=M \n";
        out += "@" + label_ +"\n";
        out += "D;JNE\n";
        return out;
    }


    string label_;
};

#endif // _BRANCH_H_