#pragma once
#define _PARSER_H_
#ifdef  _PARSER_H_

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <regex>


#define _COMMANDTYPE_
#ifdef  _COMMANDTYPE_

#define C_ARITHEMTIC 0
#define C_PUSH       1
#define C_POP        2

#define C_LABEL      3
#define C_GOTO       4
#define C_IF         5

#define C_FUNCTIONS  6
#define C_RETURN     7
#define C_CALL       8

#define C_ERROR      -1

#define ARG1_ERROR    ""
#define ARG2_ERROR   -1

#endif  //_COMMANDTYPE_

using namespace std;
vector<int*> static_reload;

//load file and remove the notions and white spaces
vector<string>& load_file(string file_name)
{
    ifstream  fin(file_name);
    string line;
    vector<string> * out = new vector<string> ();

    while (getline(fin,line))
    {  
        //cheout the first of lines word,Exclude spaces and double slashes.
        char words[2] = {line[0],line[1]}; 
        size_t pos = line.find('/');
        // the note also can contain in the end of the code
        if (words[0] == '\n' || words[0] == '\000'|| (words[0] == '/' && words[1] == '/'))
        {
            continue;
        }
        else if(pos != string::npos)
        {
            line.erase(pos);
            out->push_back(line);
        }
        else
        {
            out->push_back(line);
        }
    }
    return *out;
}

//if has more lines in the input return 1.else return 0
bool has_more_lines(vector<string> input)
{
    return input.size() == 1;
}

//split command up to three parts  
vector<string> split_command(string command)
{
    //using the regex to mapping the spaces in the text.
    regex pattern("\\s+");
    //using the sregex_token_iterator to split the command
    vector<string> tokens{sregex_token_iterator(command.begin(), command.end(), pattern, -1),
    sregex_token_iterator() };
    return tokens;

}

//check out the type of comamnd
int checkout_command_type(vector<string> command)
{
    // commandtype is C_ARITHMTIC,C_PUSH,C_POP,C_LABEL,C_GOTO,C_IF,C_FUNCTIONS,C_RETURN,C_CALL
    int command_size = command.size();
    string commandtype = command[0];

    if(command_size == 1)
    {
        // return
        if(commandtype == "return") return C_RETURN;
        // add sub and not neg it eq gt or
        else return C_ARITHEMTIC;
    }
    if(command_size == 2)
    {
        // if-goto label
        if(commandtype == "if-goto") return C_IF;
        // goto label
        if(commandtype == "goto")    return C_GOTO;
        // label label
        if(commandtype == "label")   return C_LABEL;
    }
    if(command_size == 3)
    {
        // pop local 0
        if(commandtype == "pop")  return C_POP;
        // push local 0
        if(commandtype == "push") return C_PUSH;
        // call function 0
        if(commandtype == "call") return C_CALL;
        // function function 0
        if(commandtype == "function") return C_FUNCTIONS;
    }

    return C_ERROR;

}

//return first args with is functions 
string first_args(vector<string> command)
{
    /*if commandtype is C_ARITHMTIC will return the certaintype with command,other hands,
    it will return the first args of command.and if command is C_FUNCTIONS,it's not called*/
    int command_size = command.size();
    if(command_size == 1){
        // if command is C_ARITHMTIC,return the certaintype with command
        string commandtype = command[0];
        if(commandtype == "add") return "add";  
        if(commandtype == "sub") return "sub";
        if(commandtype == "and") return "and";
        if(commandtype == "not") return "not";
        if(commandtype == "neg") return "neg";
        if(commandtype == "lt")  return  "lt";
        if(commandtype == "eq")  return  "eq";
        if(commandtype == "gt")  return  "gt";
        if(commandtype == "or")  return  "or";
        // if command is C_RETURN,return ARG1_ERROR
        return ARG1_ERROR;
    }
    else{
        return command[1];
    }
}

//return the second args in the command
int second_args(vector<string> command){
    /*should be called only if the commandtype is C_PUSH,C_POP,C_CALL or C_FUNCTIONS */
    int command_size = command.size();
    if(command_size == 3){
        // push local 0
        // pop local 0
        // call function 0
        // function function 0
        return stoi(command[2]);
    }
    return ARG2_ERROR;
}


#endif  // _PARSER_H_