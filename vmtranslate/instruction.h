#pragma once
#define _INSTRUCTION_H_
#ifdef  _INSTRUCTION_H_


#include <string>
using namespace std;


// Base class for all instructions 
class Instruction
{
public:
    virtual string output(int index) = 0;
};


#endif //_INSTRUCTION_H_