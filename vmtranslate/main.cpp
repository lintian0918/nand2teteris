#include "parser.h"
#include "coderWriter.h"
#include "cmap.h"
#include "outfile.h"

using namespace std;

// it is a VMcode translator which translate the VM code to Hack assembly code
// the input is a VM code file
// the output is a Hack assembly code file

int vmtransalte(string file_name){
    //init,and push the bootstrap code
    unique_ptr<Instruction> sys = make_unique<Bootstrap>();

    //get the floder adress
    string floder_adress = input_folder_adress(file_name);
    
    //get the output file adress
    string output_file_adress = out_file_adress(file_name);


    output_file(output_file_adress);
    
    //init the file index,which is used to generate the static memory adress
    int file_idx = 0;

    //init the artimetic and pop push instruction map
    init_map();

    //code the bootstrap code
    fout << write_instruction(sys,file_idx);

    // which is used to count the number of artimetic command,which is used to generate the jump label
    // for example, if there are 3 in gt command, the jump label wlil be GT_3
    // and the count is used to generate the Call function's return address
    // for example, if there are 3 in call command, the return address will ret$functionName_3
    int count = 0 ;

    for (string file : input_file_name(floder_adress))
    {
        //relocate the static memory adress
        map<int,int> static_map;

        // load file's command
        vector<string> input = load_file(file);

        // for each command
        for (string command : input)
        {

            //split command to three part
            vector<string> command_part = split_command(command);

            //getting the command_type in the command
            int command_type = checkout_command_type(command_part);
            
            //first second segment
            string segment = first_args(command_part);
            
            //index segmentes
            int index = second_args(command_part);

            if(segment == "static")
            {
                // if the static memory is not in the map, add it to the map
                static_map[index] = file_idx + index;

                outfile(command_type,segment,static_map[index],count,fout);
            }
            else 
            {
                outfile(command_type,segment,index,count,fout);
            }
        }
        // relocate the static memory adress
        file_idx += static_map.size();
    }
    
    close_file();
    
    return 1;
}

int main(){

    string file_name = R"(08\FunctionCalls\StaticsTest\Class2.asm)";

    vmtransalte(file_name);

    return 0;
}