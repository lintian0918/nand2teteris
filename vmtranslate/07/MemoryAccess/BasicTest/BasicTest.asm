//push constant 10
//RAM[SP] = i
@10
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop local 0
//SP--
@SP
M=M-1
//R13 = 0i
@0
D=A
@LCL
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//push constant 21
//RAM[SP] = i
@21
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push constant 22
//RAM[SP] = i
@22
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop argument 2
//SP--
@SP
M=M-1
//R13 = 2i
@2
D=A
@ARG
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//pop argument 1
//SP--
@SP
M=M-1
//R13 = 1i
@1
D=A
@ARG
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//push constant 36
//RAM[SP] = i
@36
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop this 6
//SP--
@SP
M=M-1
//R13 = 6i
@6
D=A
@THIS
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//push constant 42
//RAM[SP] = i
@42
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push constant 45
//RAM[SP] = i
@45
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop that 5
//SP--
@SP
M=M-1
//R13 = 5i
@5
D=A
@THAT
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//pop that 2
//SP--
@SP
M=M-1
//R13 = 2i
@2
D=A
@THAT
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//push constant 510
//RAM[SP] = i
@510
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop temp i 
//D = temp 6
@6
D=A
@5
D=D+A
@R13
M=D
//SP--
@SP
M=M-1
// RAM[R13] == RAM[SP] 
@SP
A=M
D=M
@R13
A=M
M=D
//push local 0
//D = RAM[local + i]
@0
D=A
@LCL
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push that 5
//D = RAM[that + i]
@5
D=A
@THAT
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//add
//SP--
@SP
M=M-1
//D = RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] + RAM[SP-1]
A=M
M=D+M
//SP++
@SP
M=M+1
//push argument 1
//D = RAM[argument + i]
@1
D=A
@ARG
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//sub
//SP--
@SP
M=M-1
//D=RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] -RAM[SP-1]
A=M
M=M-D
//SP++
@SP
M=M+1
//push this 6
//D = RAM[this + i]
@6
D=A
@THIS
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push this 6
//D = RAM[this + i]
@6
D=A
@THIS
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//add
//SP--
@SP
M=M-1
//D = RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] + RAM[SP-1]
A=M
M=D+M
//SP++
@SP
M=M+1
//sub
//SP--
@SP
M=M-1
//D=RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] -RAM[SP-1]
A=M
M=M-D
//SP++
@SP
M=M+1
//push temp 6
@6
D=A
//D = RAM[5]
@5
A=D+A
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//add
//SP--
@SP
M=M-1
//D = RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] + RAM[SP-1]
A=M
M=D+M
//SP++
@SP
M=M+1
