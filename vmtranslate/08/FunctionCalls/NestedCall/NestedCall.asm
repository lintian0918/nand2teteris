@256
D=A
@SP
M=D
@LCL
M=-1
@1
D=A
@LCL
D=M-D
@ARG
M=D
@2
D=A
@LCL
D=M-D
@THIS
M=D
@3
D=A
@LCL
D=M-D
@THAT
M=D
@ret$Sys.init
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
@LCL
D=M
@SP
A=M
M=D
//SP++
@SP
M=M+1
@ARG
D=M
@SP
A=M
M=D
//SP++
@SP
M=M+1
@THIS
D=M
@SP
A=M
M=D
//SP++
@SP
M=M+1
@THAT
D=M
@SP
A=M
M=D
//SP++
@SP
M=M+1
@SP
D=M
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.init
0;JMP
(ret$Sys.init)
// Function Sys.init0
(Sys.init)
//push constant 4000
//RAM[SP] = i
@4000
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop pointer0
//THIS OR THAT = RAM[SP--]
//SP--
@SP
M=M-1
A=M
D=M
@THIS
M=D
//push constant 5000
//RAM[SP] = i
@5000
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop pointer1
//THIS OR THAT = RAM[SP--]
//SP--
@SP
M=M-1
A=M
D=M
@THAT
M=D
// Call Sys.main0
@ret$Sys.main0
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
@LCL
D=M
@SP
A=M
M=D
//SP++
@SP
M=M+1
@ARG
D=M
@SP
A=M
M=D
//SP++
@SP
M=M+1
@THIS
D=M
@SP
A=M
M=D
//SP++
@SP
M=M+1
@THAT
D=M
@SP
A=M
M=D
//SP++
@SP
M=M+1
@SP
D=M
@5
D=D-A
@0
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.main
0;JMP
(ret$Sys.main0)
//pop temp i 
//D = temp 1
@1
D=A
@5
D=D+A
@R13
M=D
//SP--
@SP
M=M-1
// RAM[R13] == RAM[SP] 
@SP
A=M
D=M
@R13
A=M
M=D
//labelLOOP
(LOOP)
@LOOP
0;JMP
// Function Sys.main5
(Sys.main)
@SP
A=M
M=0
//SP++
@SP
M=M+1
@SP
A=M
M=0
//SP++
@SP
M=M+1
@SP
A=M
M=0
//SP++
@SP
M=M+1
@SP
A=M
M=0
//SP++
@SP
M=M+1
@SP
A=M
M=0
//SP++
@SP
M=M+1
//push constant 4001
//RAM[SP] = i
@4001
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop pointer0
//THIS OR THAT = RAM[SP--]
//SP--
@SP
M=M-1
A=M
D=M
@THIS
M=D
//push constant 5001
//RAM[SP] = i
@5001
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop pointer1
//THIS OR THAT = RAM[SP--]
//SP--
@SP
M=M-1
A=M
D=M
@THAT
M=D
//push constant 200
//RAM[SP] = i
@200
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop local 1
//SP--
@SP
M=M-1
//R13 = 1i
@1
D=A
@LCL
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//push constant 40
//RAM[SP] = i
@40
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop local 2
//SP--
@SP
M=M-1
//R13 = 2i
@2
D=A
@LCL
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//push constant 6
//RAM[SP] = i
@6
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop local 3
//SP--
@SP
M=M-1
//R13 = 3i
@3
D=A
@LCL
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//push constant 123
//RAM[SP] = i
@123
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
// Call Sys.add121
@ret$Sys.add121
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
@LCL
D=M
@SP
A=M
M=D
//SP++
@SP
M=M+1
@ARG
D=M
@SP
A=M
M=D
//SP++
@SP
M=M+1
@THIS
D=M
@SP
A=M
M=D
//SP++
@SP
M=M+1
@THAT
D=M
@SP
A=M
M=D
//SP++
@SP
M=M+1
@SP
D=M
@5
D=D-A
@1
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.add12
0;JMP
(ret$Sys.add121)
//pop temp i 
//D = temp 0
@0
D=A
@5
D=D+A
@R13
M=D
//SP--
@SP
M=M-1
// RAM[R13] == RAM[SP] 
@SP
A=M
D=M
@R13
A=M
M=D
//push local 0
//D = RAM[local + i]
@0
D=A
@LCL
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push local 1
//D = RAM[local + i]
@1
D=A
@LCL
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push local 2
//D = RAM[local + i]
@2
D=A
@LCL
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push local 3
//D = RAM[local + i]
@3
D=A
@LCL
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push local 4
//D = RAM[local + i]
@4
D=A
@LCL
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//add
//SP--
@SP
M=M-1
//D = RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] + RAM[SP-1]
A=M
M=D+M
//SP++
@SP
M=M+1
//add
//SP--
@SP
M=M-1
//D = RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] + RAM[SP-1]
A=M
M=D+M
//SP++
@SP
M=M+1
//add
//SP--
@SP
M=M-1
//D = RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] + RAM[SP-1]
A=M
M=D+M
//SP++
@SP
M=M+1
//add
//SP--
@SP
M=M-1
//D = RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] + RAM[SP-1]
A=M
M=D+M
//SP++
@SP
M=M+1
//Return
@LCL
D=M
@R13
M=D
@R13
D=M
@5
D=D-A
A=D
D=M
@R14
M=D
//SP--
@SP
M=M-1
@SP
A=M
D=M
@ARG
A=M
M=D
@ARG
D=M+1
@SP
M=D
@R13
D=M
@1
D=D-A
A=D
D=M
@THAT
M=D
@R13
D=M
@2
D=D-A
A=D
D=M
@THIS
M=D
@R13
D=M
@3
D=D-A
A=D
D=M
@ARG
M=D
@R13
D=M
@4
D=D-A
A=D
D=M
@LCL
M=D
@R14
A=M
0;JMP
// Function Sys.add120
(Sys.add12)
//push constant 4002
//RAM[SP] = i
@4002
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop pointer0
//THIS OR THAT = RAM[SP--]
//SP--
@SP
M=M-1
A=M
D=M
@THIS
M=D
//push constant 5002
//RAM[SP] = i
@5002
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop pointer1
//THIS OR THAT = RAM[SP--]
//SP--
@SP
M=M-1
A=M
D=M
@THAT
M=D
//push argument 0
//D = RAM[argument + i]
@0
D=A
@ARG
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push constant 12
//RAM[SP] = i
@12
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//add
//SP--
@SP
M=M-1
//D = RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] + RAM[SP-1]
A=M
M=D+M
//SP++
@SP
M=M+1
//Return
@LCL
D=M
@R13
M=D
@R13
D=M
@5
D=D-A
A=D
D=M
@R14
M=D
//SP--
@SP
M=M-1
@SP
A=M
D=M
@ARG
A=M
M=D
@ARG
D=M+1
@SP
M=D
@R13
D=M
@1
D=D-A
A=D
D=M
@THAT
M=D
@R13
D=M
@2
D=D-A
A=D
D=M
@THIS
M=D
@R13
D=M
@3
D=D-A
A=D
D=M
@ARG
M=D
@R13
D=M
@4
D=D-A
A=D
D=M
@LCL
M=D
@R14
A=M
0;JMP
