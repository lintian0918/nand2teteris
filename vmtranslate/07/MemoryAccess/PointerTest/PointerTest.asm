//push constant 3030
//RAM[SP] = i
@3030
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop pointer0
//THIS OR THAT = RAM[SP--]
//SP--
@SP
M=M-1
A=M
D=M
@THIS
M=D
//push constant 3040
//RAM[SP] = i
@3040
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop pointer1
//THIS OR THAT = RAM[SP--]
//SP--
@SP
M=M-1
A=M
D=M
@THAT
M=D
//push constant 32
//RAM[SP] = i
@32
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop this 2
//SP--
@SP
M=M-1
//R13 = 2i
@2
D=A
@THIS
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//push constant 46
//RAM[SP] = i
@46
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop that 6
//SP--
@SP
M=M-1
//R13 = 6i
@6
D=A
@THAT
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//push pointer0
//RAM[SP] = THIS OR THAT
@THIS
D=M
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push pointer1
//RAM[SP] = THIS OR THAT
@THAT
D=M
@SP
A=M
M=D
//SP++
@SP
M=M+1
//add
//SP--
@SP
M=M-1
//D = RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] + RAM[SP-1]
A=M
M=D+M
//SP++
@SP
M=M+1
//push this 2
//D = RAM[this + i]
@2
D=A
@THIS
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//sub
//SP--
@SP
M=M-1
//D=RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] -RAM[SP-1]
A=M
M=M-D
//SP++
@SP
M=M+1
//push that 6
//D = RAM[that + i]
@6
D=A
@THAT
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//add
//SP--
@SP
M=M-1
//D = RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] + RAM[SP-1]
A=M
M=D+M
//SP++
@SP
M=M+1
