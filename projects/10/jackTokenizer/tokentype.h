#pragma once 

#define _TOKENTYPE_H_
#ifdef  _TOKENTYPE_H_

#include <string>
#include <iostream>


using namespace std;

// base class for all token types
class TokenTypes
{
    public:
    virtual string output(string token) = 0;
    virtual string get_value() = 0;
};

class Keyword: public TokenTypes
{
    public:
    string output(string token) override
    {
        return " ";
    }
    string get_value() override
    {
        return value;
    }
    Keyword(string value): value(value) {}
    string value;
};

class Symbol: public TokenTypes
{
    public:
    string output(string token) override
    {
        return "";
    }
    string get_value() override
    {
        return value;
    }

    Symbol(string value): value(value) {}
    string value;
    
};

class Identifier: public TokenTypes
{
    public:
    string output(string token) override
    {
        return "";

    } 
    string get_value() override
    {
        return value;
    }
    Identifier(string value): value(value) {}
    string value;
};

class IntegerConstant: public TokenTypes
{
    public:
    string output(string token) override
    {
        return "";

    }
    string get_value() override
    {
        return value;
    }
    IntegerConstant(string value): value(value) {}
    string value;
    
};

class StringConstant: public TokenTypes
{
    public:
    string output(string token) override
    {
        return "";

    }
    string get_value() override
    {
        return value;
    }
    StringConstant(string value): value(value) {}
    string value;
    
};

#endif // _TOKENTYPE_H_