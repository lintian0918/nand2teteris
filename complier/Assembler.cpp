#include "code.h"
#include "parse.h"



int main(){
    
    string file_name{"06/pong/Pong.asm"};
    vector<vector<string>> tokens = parse(file_name);     //load file
    
    ofstream outfile("Pong.hack");                        //open a file
    if (outfile.is_open()){
        for (vector<string> ins : tokens)
        {
            string out;
            int size = ins.size();

            //convert code to binary
            if(size == 1) { out = code_a_instruction(ins);}
            else {out = code_c_instruction(ins);}
            
            outfile << out << endl;                       //write in file
        }

        outfile.close();                                  //close file
    }
}


