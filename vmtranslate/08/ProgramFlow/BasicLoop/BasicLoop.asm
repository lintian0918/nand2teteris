//push constant 0
//RAM[SP] = i
@0
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop local 0
//SP--
@SP
M=M-1
//R13 = 0i
@0
D=A
@LCL
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//labelLOOP_START
(LOOP_START)
//push argument 0
//D = RAM[argument + i]
@0
D=A
@ARG
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push local 0
//D = RAM[local + i]
@0
D=A
@LCL
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//add
//SP--
@SP
M=M-1
//D = RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] + RAM[SP-1]
A=M
M=D+M
//SP++
@SP
M=M+1
//pop local 0
//SP--
@SP
M=M-1
//R13 = 0i
@0
D=A
@LCL
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//push argument 0
//D = RAM[argument + i]
@0
D=A
@ARG
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push constant 1
//RAM[SP] = i
@1
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//sub
//SP--
@SP
M=M-1
//D=RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] -RAM[SP-1]
A=M
M=M-D
//SP++
@SP
M=M+1
//pop argument 0
//SP--
@SP
M=M-1
//R13 = 0i
@0
D=A
@ARG
D=D+M
@R13
M=D
//RAM[R13] = RAM[SP]
@SP
A=M
D=M
@R13
A=M
M=D
//push argument 0
//D = RAM[argument + i]
@0
D=A
@ARG
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//if-gotoLOOP_START
//SP--
@SP
M=M-1
@SP
A=M
D=M 
@LOOP_START
D;JNE
//push local 0
//D = RAM[local + i]
@0
D=A
@LCL
A=D+M
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
