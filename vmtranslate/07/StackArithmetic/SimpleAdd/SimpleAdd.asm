//push constant 7
//RAM[SP] = i
@7
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push constant 8
//RAM[SP] = i
@8
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//add
//SP--
@SP
M=M-1
//D = RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] + RAM[SP-1]
A=M
M=D+M
//SP++
@SP
M=M+1
