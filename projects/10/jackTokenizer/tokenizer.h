#pragma once
#define _TOKENIZER_H_
#ifdef  _TOKENIZER_H_

#include <string>
#include <vector>
#include <fstream>
#include <regex>
#include "tokentype.h"
#include <algorithm>
#include <map>
#include <stack>
#include <set>


using namespace std;


class Tokenizer{
public:
    Tokenizer(TokenTypes *token_type): token_type_(token_type) {}
    ~Tokenizer() {delete token_type_;}

    void setting_token_type(TokenTypes *token_type)
    {
        delete token_type_;
        token_type_ = token_type;
    }

    void tokenize(string token)
    {
        token_type_->output(token);
    }

private:
    TokenTypes *token_type_;
};

vector<string> load_file(string path) {
    ifstream fin(path);
    string line;
    vector<string> out;
    bool in_multiline_comment = false;


    while (getline(fin, line)) {

        // check if the line is empty
        size_t space = line.find_first_not_of(" \t");
        if (space != string::npos) {
            line = line.substr(space);
        }
        // check if the line is a comment
        size_t single_line_comment_pos = line.find("//");
        size_t multiline_start_pos = line.find("/*");
        size_t multiline_end_pos = line.find("*/");

        // check if the line is a multiline comment
        bool is_singule = multiline_start_pos != string::npos && multiline_end_pos != string::npos;
        if(is_singule && multiline_start_pos < multiline_end_pos) {
            // remove the comment
            line = line.substr(0, multiline_start_pos) + line.substr(multiline_end_pos + 2);
        }
        // check if the line is a multiline comment
        else if(in_multiline_comment) {

            if (multiline_end_pos != string::npos) {
                // remove the comment
                in_multiline_comment = false;
                line = line.substr(multiline_end_pos + 2);
            } else {

                continue;
            }
        }
        else if (multiline_start_pos != string::npos) {
            in_multiline_comment = true;
            line = line.substr(0, multiline_start_pos);
        }

        else if (single_line_comment_pos != string::npos) {
            line = line.substr(0, single_line_comment_pos);
        }

        else if (!line.empty() && line != "\t") {
            out.push_back(line);
        }
    }
    return out;
}

vector<string> parser_token(string& input)
{
    std::string delimiters = R"([ +\-*/&|<>=~\[\]{}().,;])";
    // use the regex to split the string, and return a vector of tokens
    std::vector<std::string> result;
    std::regex re(R"((\d+)|())" + delimiters);

    // In the {-1,0,1}, -1 means to match the text between the delimiters
    // 0 means to macth the first part of the delimiter
    // 1 means to match the second part of the delimiter 
    std::sregex_token_iterator it(input.begin(), input.end(), re, {-1, 0, 1});
    std::sregex_token_iterator reg_end;

    for (; it != reg_end; ++it) {
        if (!it->str().empty()) {
            result.push_back(it->str());
        }
    }

    // remove the space
    result.erase(remove(result.begin(), result.end(), " "), result.end());
    return result;
}

bool has_more_tkoens(vector<string> &tokens)
{
    return !tokens.empty();
}

vector<string> get_tokens(string path)
{
    vector<string> file = load_file(path);
    vector<string> tokens;
    for (auto line: file) {
        // tokenize the line
        vector<string> token = parser_token(line);
        // add the tokens to the vector
        tokens.insert(tokens.end(), token.begin(), token.end());
    }
    return tokens;
}



set<string> symbol = {
    "{", "}", "(", ")", "[", "]", ".", ",", ";", "+", "-", "*", "/", "&", "|", "<", ">", "=", "~"
};

set<string> keyword = {
    "class", "constructor", "function", "method", "field", "static", "var", "int", "char", "boolean", "void", "true", "false", "null", "this", "let", "do", "if", "else", "while", "return"
};

stack<string> string_constant;


shared_ptr<TokenTypes> token_type_factory(string token)
{
    bool is_keyword = keyword.find(token) != keyword.end();
    bool is_symbol = symbol.find(token) != symbol.end();
    bool is_string = token[0] == '"';
    bool is_string_end = token[token.size() - 1] == '"';
    bool is_integer = token[0] >= '0' && token[0] <= '9';

    if(is_string && !is_string_end){
        string_constant.push(token);
        shared_ptr<TokenTypes> ptr = make_shared<StringConstant>(" ");
        return ptr;
    }
    if(is_string_end){
        for(;!(string_constant.size() == 1);){
            token = string_constant.top() + token;
            string_constant.pop();
        }
        shared_ptr<TokenTypes> ptr_string = make_shared<StringConstant>(token);
        return ptr_string;
    }
    if (is_keyword) {
        shared_ptr<TokenTypes> ptr_keyword = make_shared<Keyword>(token);
        return ptr_keyword;
    }
    if (is_symbol) {
        shared_ptr<TokenTypes> ptr_symbol = make_shared<Symbol>(token);
        return ptr_symbol;
    }
    if (is_integer) {
        shared_ptr<TokenTypes> ptr_int = make_shared<IntegerConstant>(token);
        return ptr_int;
    }
    shared_ptr<TokenTypes> ptr_ident = make_shared<Identifier>(token);
    return ptr_ident;
}

#endif  // _TOKENIZER_H_