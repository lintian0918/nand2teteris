#pragma once
#define _CONSTANTS_H_
#ifdef _CONSTANTS_H_

#include <string>

using namespace std;

//token types
const string KEYWORD = "keyword";
const string IDENTIFIER = "identifier";
const string SYMBOL = "symbol";
const string INT_CONST = "integerConstant";
const string STRING_CONST = "stringConstant";



//keyword constants
const string CLASS = "class";
const string METHOD = "method";
const string FUNCTION = "function";
const string CONSTRUCTOR = "constructor";
const string INT = "int";
const string BOOLEAN = "boolean";
const string CHAR = "char";
const string VOID = "void";
const string VAR = "var";
const string STATIC = "static";
const string FIELD = "field";
const string LET = "let";
const string DO = "do";
const string IF = "if";
const string ELSE = "else";
const string WHILE = "while";
const string RETURN = "return";
const string TRUE = "true";
const string FALSE = "false";
const string NULL_JACK = "null";
const string THIS = "this";




#endif // _CONSTANTS_H_