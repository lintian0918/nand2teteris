//push constant 111
//RAM[SP] = i
@111
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push constant 333
//RAM[SP] = i
@333
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push constant 888
//RAM[SP] = i
@888
D=A
@SP
A=M
M=D
//SP++
@SP
M=M+1
//pop static i 
//D = pop static 8
@8
D=A
@16
D=D+A
@R13
M=D
//SP--
@SP
M=M-1
// RAM[R13] == RAM[SP] 
@SP
A=M
D=M
@R13
A=M
M=D
//pop static i 
//D = pop static 3
@3
D=A
@16
D=D+A
@R13
M=D
//SP--
@SP
M=M-1
// RAM[R13] == RAM[SP] 
@SP
A=M
D=M
@R13
A=M
M=D
//pop static i 
//D = pop static 1
@1
D=A
@16
D=D+A
@R13
M=D
//SP--
@SP
M=M-1
// RAM[R13] == RAM[SP] 
@SP
A=M
D=M
@R13
A=M
M=D
//push static 3
@3
D=A
//D = RAM[16]
@16
A=D+A
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//push static 1
@1
D=A
//D = RAM[16]
@16
A=D+A
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//sub
//SP--
@SP
M=M-1
//D=RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] -RAM[SP-1]
A=M
M=M-D
//SP++
@SP
M=M+1
//push static 8
@8
D=A
//D = RAM[16]
@16
A=D+A
D=M
//RAM[SP] = D
@SP
A=M
M=D
//SP++
@SP
M=M+1
//add
//SP--
@SP
M=M-1
//D = RAM[SP]
A=M
D=M
//SP--
@SP
M=M-1
//RAM[SP-1] = RAM[SP] + RAM[SP-1]
A=M
M=D+M
//SP++
@SP
M=M+1
