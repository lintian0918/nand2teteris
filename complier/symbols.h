/*this part is to deal with the symbols,symbols are divided two part.
first part is called line symbols. we will scan twice,first scan is order to push the next line number 
of symbols to a vector.second part is called the variable symbol.It represent a memory adress or
a line number .the second scan will compared the symbool to the map which contain the preset number.
if mapping is false,it will to map a number are began 16.finally this file will output a vector which 
contain whole number in files*/


#pragma once 
#define _SYMBOLS_H_
#ifdef  _SYMBOLS_H_

#include <string>
#include <vector>
#include <map>
#include <regex>
#include <iostream>
#include <algorithm>


using namespace std;

map<string,string> var_symbol_map;

map<string,string> line_symbol_map;

map<string,string> preset_symbols_map{
 {"R0","0"},{"R1","1"},{"R2","2"},{"R3","3"}
,{"R4","4"},{"R5","5"},{"R6","6"},{"R7","7"}
,{"R8","8"},{"R9","9"},{"R10","10"},{"R11","11"}
,{"R12","12"},{"R13","13"},{"R14","14"},{"R15","15"}
,{"SP","0"},{"LCL","1"},{"ARG","2"},{"THIS","3"},{"THAT","4"}
,{"SCREEN","16384"},{"KBD","24576"}};


//check out the string wether is number string
bool is_umnber_string(const std::string& s) {
    for (char c : s) {
        if (!isdigit(c)) {
            return false;
        }
    }
    return true;
}

/*input raw text,output the text which remove the line symbols*/
vector<string> line_symbols_sacn(vector<string> raw_text)
{
    //output text
    vector<string> text = raw_text; 
    
    for(int i=0; i < text.size(); i++){
        //mapping (SYMBOLS) to match
        regex regex("\\((.*?)\\)");
        smatch match;
        string line = text[i];

        //check result is empty or ont empty
        if (regex_search(line, match, regex)) {
            string key = match[0];
            key.erase(remove(key.begin(),key.end(),'('));
            key.erase(remove(key.begin(),key.end(),')'));
            line_symbol_map[key] = to_string(i);
            text.erase(text.begin()+i);
            i--;

        } else {
            continue;
        }
    }
    return text;
}

//second scan,it will be replace all of the symbols
vector<string> preset_symbol_scan(vector<string> removed_line_symbol_text)
{
    
    
    vector<string> out = removed_line_symbol_text;
    //if the table is a variable,and allocate it to a memory
    int memory = 16;
    
    
    for(int i=0; i< out.size(); i++)
    {
        //for each line,just need to deal with the a-ins
        string line = out[i];
        if(line[0] == '@'){
            //remove "@" 
            line.erase(0,1);
            
            bool is_preset = preset_symbols_map[line] == "";
            bool is_line_symbol = line_symbol_map[line] == "";
            bool is_var_symbol = var_symbol_map[line] =="";
            bool is_num =is_umnber_string(line);
            
            if(!is_preset)   //if it's preset
            {
                string new_line = '@' + preset_symbols_map[line];
                out[i] = new_line;
            }
            else if(!is_line_symbol){
                // if it's line_symbol
                string new_line = '@' + line_symbol_map[line];
                out[i] = new_line;
            }
            else if(!is_var_symbol){
                //if it's variable are defined before
                string new_line = '@' + var_symbol_map[line];
                out[i] = new_line;
            }
            else if(!is_num){
                //if it is a number do nothing,but a symbols allocate a memory with them.
                var_symbol_map[line] = to_string(memory);
                string new_line = '@' + to_string(memory);
                out[i] = new_line;
                memory+=1;
            }
        }
    }
    return out;
}

#endif  //_SYMBOLS_H_
