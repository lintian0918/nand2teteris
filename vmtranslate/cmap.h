#pragma once 
#define _CMAP_H_
#ifdef  _CMAP_H_

#include "pushpop.h"
#include "arthmetic.h"
#include "branch.h"
#include "parser.h"
#include "coderWriter.h"
#include "functioncall.h"
#include "bootstrap.h"

#include <map>
#include <string>
#include <memory>

using namespace std;

// the all of the instructions are stored in a map
// the key is the command type
// the value is the instruction object

map<string,unique_ptr<Instruction>> arthmetic_map;
map<string,unique_ptr<Instruction>> push_map;
map<string,unique_ptr<Instruction>> pop_map;
map<int,unique_ptr<Instruction>> label_map;
map<int,unique_ptr<Instruction>> function_map;


void init_map(){
    arthmetic_map["add"] = make_unique<Add>();
    arthmetic_map["sub"] = make_unique<Sub>();
    arthmetic_map["neg"] = make_unique<Neg>();
    arthmetic_map["eq"] = make_unique<Eq>();
    arthmetic_map["gt"] = make_unique<Gt>();
    arthmetic_map["lt"] = make_unique<Lt>();
    arthmetic_map["and"] = make_unique<And>();
    arthmetic_map["or"] = make_unique<Or>();
    arthmetic_map["not"] = make_unique<Not>();

    push_map["local"] = make_unique<PushLocal>();
    push_map["argument"] = make_unique<PushArgument>();
    push_map["this"] = make_unique<PushThis>();
    push_map["that"] = make_unique<PushThat>();
    push_map["static"] = make_unique<PushStatic>();
    push_map["temp"] = make_unique<PushTemp>();
    push_map["pointer"] = make_unique<PushPointer>();
    push_map["constant"] = make_unique<PushConstant>();

    pop_map["local"] = make_unique<PopLocal>();
    pop_map["argument"] = make_unique<PopArgument>();
    pop_map["this"] = make_unique<PopThis>();
    pop_map["that"] = make_unique<PopThat>();
    pop_map["static"] = make_unique<PopStatic>();
    pop_map["temp"] = make_unique<PopTemp>();
    pop_map["pointer"] = make_unique<PopPointer>();
}

void map_label_label(string label)
{
    label_map[C_LABEL] = make_unique<LabelLabel>(label);
}

void map_label_goto(string label)
{
    label_map[C_GOTO]  = make_unique<GotoLabel>(label);
}

void map_label_if(string label)
{
    label_map[C_IF]    = make_unique<IfGotoLabel>(label);
}

void map_function_function(string function_name)
{
    function_map[C_FUNCTIONS] = make_unique<Function>(function_name);
}

void map_function_call(string function_name,int id)
{
    function_map[C_CALL] = make_unique<Call>(function_name,id);
}

void map_function_return()
{
    function_map[C_RETURN] = make_unique<Return>();
}
#endif //_MAP_H_