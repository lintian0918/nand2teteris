#pragma once

#define _CODE_H_
#ifdef  _CODE_H_

#include "parse.h"
#include <map>

#define _PART_CONST_
#ifdef  _PART_CONST_

#define DEST_PART           0
#define COMP_PART           1
#define JUMP_PART           2

#define INS_MAX_SIZE        3
#define INS_MIDDLE_SIZE     2

#define HAVE_JUMP_PART      1
#define HAVE_COMP_PART      0

#define HEADINS "111"

#endif //_PART_CONST_

using namespace std;



string code_a_instruction(vector<string> instrcution)
{
    //a-ins only one element
    string line = instrcution[0];

    //"@number"
    //char type num  to int type num
    line.erase(0,1);
    int num = stoi(line);

    //std::biset translate to std::string
    bitset<16> binary(num); 
    string out = binary.to_string();
    return out;

}

string code_c_instruction(vector<string> instruction)
{

    /*c-ins consists of three parts,
    dest = comp ; jump
    if the length of ins is three,just sequentially convert to binary
    but if the length of ins is 2, we have to discuss in instance follwing by :
    No dest-part OR No jump-part.
    if no dest-part,relace them with zero,if no jump-part,replace them with zero too.
    */
    
    //ture table of mapping
    map<string,string> dic_comp;
    map<string,string> dic_dest;
    map<string,string> dic_jump;
    
    dic_comp["0"]    = "0101010";
    dic_comp["1"]    = "0111111";
    dic_comp["-1"]   = "0111010";
    dic_comp["D"]    = "0001100";
    dic_comp["A"]    = "0110000";
    dic_comp["!D"]   = "0001101";
    dic_comp["!A"]   = "0110001";
    dic_comp["-D"]   = "0001111";
    dic_comp["-A"]   = "0110011";
    dic_comp["D+1"]  = "0011111";
    dic_comp["A+1"]  = "0110111";
    dic_comp["D-1"]  = "0001110";
    dic_comp["A-1"]  = "0110010";
    dic_comp["D+A"]  = "0000010";
    dic_comp["D-A"]  = "0010011";
    dic_comp["A-D"]  = "0000111";
    dic_comp["D&A"]  = "0000000";
    dic_comp["D|A"]  = "0010101";
    dic_comp["M"]    = "1110000"; 
    dic_comp["!M"]   = "1110001";
    dic_comp["-M"]   = "1110011";
    dic_comp["M+1"]  = "1110111";
    dic_comp["M-1"]  = "1110010";
    dic_comp["D+M"]  = "1000010";
    dic_comp["D-M"]  = "1010011";
    dic_comp["M-D"]  = "1000111";
    dic_comp["D&M"]  = "1000000";
    dic_comp["D|M"]  = "1010101";

    dic_dest["null"] = "000";
    dic_dest["M"]    = "001";
    dic_dest["D"]    = "010";
    dic_dest["MD"]   = "011";
    dic_dest["A"]    = "100";
    dic_dest["AM"]   = "101";
    dic_dest["AD"]   = "110";
    dic_dest["AMD"]  = "111";

    dic_jump["null"] = "000";
    dic_jump["JGT"]  = "001";
    dic_jump["JEQ"]  = "010";
    dic_jump["JGE"]  = "011";
    dic_jump["JLT"]  = "100";
    dic_jump["JNE"]  = "101";
    dic_jump["JLE"]  = "110";
    dic_jump["JMP"]  = "111";


    int size = instruction.size();

    string control_part; 
    string dest_part;
    string jump_part;

    if(size == INS_MAX_SIZE){
        //fill in order
        dest_part    = dic_dest[instruction[DEST_PART]];
        control_part = dic_comp[instruction[COMP_PART]];
        jump_part    = dic_jump[instruction[JUMP_PART]];
    }
    if(size == INS_MIDDLE_SIZE ){
        bool test = dic_jump[instruction[HAVE_JUMP_PART]] == "";
        // do a test on jump_part,if there is a jump instruction,then let the dest_part be zero,and so on
        if(test == true){
            dest_part    = dic_dest[instruction[DEST_PART]];
            control_part = dic_comp[instruction[COMP_PART]];
            jump_part    = dic_jump["null"];
        } 
        else{
            dest_part    = dic_dest["null"];
            control_part = dic_comp[instruction[HAVE_COMP_PART]];
            jump_part    = dic_jump[instruction[HAVE_JUMP_PART]];
        }
    }
    string out = HEADINS + control_part + dest_part + jump_part;

    return out;
    
}

#endif // _CODE_H_