#pragma once 
#define _OUTFILE_H_
#ifdef  _OUTFILE_H_
// this file is used to output the assembly code

#define output_file(file_name) ofstream fout(file_name)
#define close_file() fout.close()

#include <string>
#include <fstream>
#include <algorithm>
#include <filesystem>

using namespace std;

// output file's name
string out_file_adress(string file_path)
{
    size_t last_slash_idx = file_path.find_last_of(".");
    string current_dir = file_path.substr(0, last_slash_idx) + ".asm";
    return R"()" + current_dir + R"()";
}

// get all the files adress in the folder
string input_folder_adress(string path)
{
    size_t last_slash_idx = path.find_last_of("\\/");
    string current_dir = path.substr(0, last_slash_idx);

    return R"()" + current_dir + R"()";
}

// read all the files in the folder,and return a vector of file names
vector<string> input_file_name(string folder_path) {
    vector<string> file_names;
    for (const auto &entry : std::filesystem::directory_iterator(folder_path)) {
        if (entry.is_regular_file() && entry.path().extension() == ".vm")
        {
            // if the file is Sys.vm, put it in the front of the vector
            if (entry.path().filename() == "Sys.vm")
            {
                file_names.insert(file_names.begin(), entry.path().string());
            }
            else
            {
                file_names.push_back(entry.path().string());
            }
        }
    }

    return file_names;
}

// fout, the output ofstream
void outfile(int command_type, string segment, int index,int& count,ofstream& fout)
{
        if     (command_type == C_ARITHEMTIC)
        {
            fout << write_instruction(arthmetic_map[segment],count);
            count++;
        }
        else if(command_type == C_PUSH) 
        {
            fout << write_instruction(push_map[segment],index);
        }
        else if(command_type == C_POP)
        {
            fout << write_instruction(pop_map[segment],index);
        }
        else if(command_type == C_LABEL)
        {
            map_label_label(segment);
            fout << write_instruction(label_map[C_LABEL],index);
        }
        else if(command_type == C_IF)
        {
            map_label_if(segment);
            fout << write_instruction(label_map[C_IF],index);

        }
        else if(command_type == C_GOTO)
        {
            map_label_goto(segment);
            fout << write_instruction(label_map[C_GOTO],index);
        }
        else if(command_type == C_FUNCTIONS)
        {
            map_function_function(segment);
            fout << write_instruction(function_map[C_FUNCTIONS],index);
        }
        else if(command_type == C_RETURN)
        {
            map_function_return();
            fout << write_instruction(function_map[C_RETURN],index);

        }
        else if(command_type == C_CALL)
        {
            map_function_call(segment,count);
            count++;
            fout << write_instruction(function_map[C_CALL],index);
        }
}

#endif //OUTFILE_H