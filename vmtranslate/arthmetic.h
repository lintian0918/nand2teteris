#pragma once
#define _ARTHMETIC_H
#ifdef _ARTHMETIC_H

#include <string>
#include "instruction.h"
using namespace std;

#define SPUP "//SP++\n@SP\nM=M+1\n"
#define SPDM "//SP--\n@SP\nM=M-1\n"


/*arthmetic is a sort of instruction of the languange,here are all of the implementation*/
//the Arthmetic class is the base class of all arthmetic instructions
//class has a function output,which will return the assembly code of the instruction

// Base class for all arthmetic instructions
class Arthmetic :public Instruction
{
public:
    virtual string output(int index) = 0;
};

class Add :public Arthmetic
{
public:
    string output (int index) override
    {
        string out = "";
        out+="//add\n";
        //load a variable of the SP
        out+=SPDM;
        out+="//D = RAM[SP]\n";
        out+="A=M\n";
        out+="D=M\n";
        out+=SPDM;
        // to do function add
        out+="//RAM[SP-1] = RAM[SP] + RAM[SP-1]\n";
        out+="A=M\n";
        out+="M=D+M\n";
        out+=SPUP;
        return out;   
    }
};

class Sub :public Arthmetic
{
public:
    string output(int index) override
    {
        string out = "";
        out +="//sub\n";
        //load a variable of the SP
        out +=SPDM;
        out +="//D=RAM[SP]\n";
        out +="A=M\n";
        out +="D=M\n";

        out +=SPDM;
        out +="//RAM[SP-1] = RAM[SP] -RAM[SP-1]\n";
        out +="A=M\n";
        out +="M=M-D\n";
        out +=SPUP;
        return out;
    }
};

class Neg :public Arthmetic
{
public:
    string output(int index) override
    {
        string out = "";
        out +="//neg\n";
        out +=SPDM;
        out +="A=M\n";
        out +="M=-M\n";
        out +=SPUP;
        return out;
    }
};

class Eq :public Arthmetic
{
    public:
    string output(int index) override
    {
        string str_idx = to_string(index);
        string out = "";
        out +="//eq\n";

        out +=SPDM;
        out +="A=M\n";
        out +="D=M\n";
        
        out +=SPDM;
        out +="A=M\n";
        // if D-M is 0 and jump to JP_1
        out +="D=M-D\n";
        out +="@EQ_1" + str_idx + "\n";
        out +="D;JEQ\n";
        
        // if D-M not 0 jump to JP_2
        out +="@SP\n";
        out +="A=M\n";
        out +="M=0\n";

        out +="@EQ_2" + str_idx + "\n";
        out +="0;JEQ\n";
        
        out +="(EQ_1" + str_idx + ")" + "\n";
        out +="@SP\n";
        out +="A=M\n";
        out +="M=-1\n";
        
        //load the memory 
        out +="(EQ_2" + str_idx + ")" + "\n";
        out +=SPUP;
        return out;
    }
};

class Lt :public Arthmetic
{
public:
    string output(int index) override
    {
        string str_idx = to_string(index);
        string out = "";
        out +="//lt\n";

        out +=SPDM;
        out +="A=M\n";
        out +="D=M\n";
        
        out +=SPDM;
        out +="A=M\n";
        // if D-M is 0 and jump to JP_1
        out +="D=M-D\n";
        out +="@LT_1" + str_idx + "\n";
        out +="D;JLT\n";
        
        // if D-M not 0 jump to JP_2
        out +="@SP\n";
        out +="A=M\n";
        out +="M=0\n";

        out +="@LT_2" + str_idx + "\n";
        out +="0;JEQ\n";
        
        out +="(LT_1" + str_idx + ")" + "\n";
        out +="@SP\n";
        out +="A=M\n";
        out +="M=-1\n";
        
        //load the memory 
        out +="(LT_2" + str_idx + ")" + "\n";
        out +=SPUP;
        return out;
    }
};

class Gt :public Arthmetic
{
    string output(int index) override
    {
        string str_idx = to_string(index);
        string out = "";
        out +="//gt\n";

        out +=SPDM;
        out +="A=M\n";
        out +="D=M\n";
        
        out +=SPDM;
        out +="A=M\n";
        // if D-M is 0 and jump to JP_1
        out +="D=M-D\n";
        out +="@GT_1" + str_idx + "\n";
        out +="D;JGT\n";
        
        // if D-M not 0 jump to JP_2
        out +="@SP\n";
        out +="A=M\n";
        out +="M=0\n";

        out +="@GT_2" + str_idx + "\n";
        out +="0;JEQ\n";
        
        out +="(GT_1" + str_idx + ")" + "\n";
        out +="@SP\n";
        out +="A=M\n";
        out +="M=-1\n";
        
        //load the memory 
        out +="(GT_2" + str_idx + ")" + "\n";
        out +=SPUP;
        return out;
    }
};

class And :public Arthmetic
{
    string output(int index) override
    {
        string out = "";
        out +="//and\n";
        //SP--
        //load a variable of the SP
        out +=SPDM;
        out +="A=M\n";
        out +="D=M\n";
        out +=SPDM;
        // to do function add
        out +="A=M\n";
        out +="M=D&M\n";
        out +=SPUP;
        return out;
    }
};

class Or :public Arthmetic
{
    string output(int index) override
    {
        string out = "";
        out +="//or\n";
        //SP--
        //load a variable of the SP
        out +=SPDM;
        out +="A=M\n";
        out +="D=M\n";
        out +=SPDM;
        // to do function add
        out +="A=M\n";
        out +="M=D|M\n";
        out +=SPUP;
        return out;
    }
};

class Not :public Arthmetic
{
    string output(int index) override
    {
        string out = "";
        out +="//not\n";
        out +=SPDM;
        out +="A=M\n";
        out +="M=!M\n";
        out +=SPUP;
        return out;
    }
};

#endif // _ARTHMETIC_H